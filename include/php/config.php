<?php
//define directories
define("ROOT", $_SERVER['DOCUMENT_ROOT'] . "/trainers");
define("DIR_INCLUDE", ROOT . "/include");
define("DIR_CSS", DIR_INCLUDE . "/css");
define("DIR_JS", DIR_INCLUDE . "/js");
define("DIR_IMG", DIR_INCLUDE . "/images");
define("DIR_PHP", DIR_INCLUDE . "/php");
define("DIR_VENDORS", DIR_INCLUDE . "/vendors");
define("DIR_VIEWS", ROOT . "/views");
define("DIR_DATA_TABLES", DIR_INCLUDE . "/datatables");
define("DIR_UPLOADS", DIR_INCLUDE . "/uploads");
//define Web Paths
define("SITE_URL", "http://trainers.ablesafety.com");
define("URL_INCLUDE", SITE_URL . "/include");
define("URL_CSS", URL_INCLUDE . "/css");
define("URL_JS", URL_INCLUDE . "/js");
define("URL_IMG", URL_INCLUDE . "/images");
define("URL_PHP", URL_INCLUDE . "/php");
define("URL_VENDORS", URL_INCLUDE . "/vendors");
define("URL_VIEWS", SITE_URL . "/views");
define("URL_UPLOADS", URL_INCLUDE . "/uploads");
define("URL_DATA_TABLES", URL_INCLUDE . "/datatables");

//define database information
define("DB_NAME", 'ablesafety');
define("DB_USER", 'ablesafety');
define("DB_PWD", 'Titan1234!');
define("DB_HOST", 'ablesafety.db.5337388.hostedresource.com');

include DIR_PHP . '/functions.php';

$asdb = creatDatabaseConnection();
$color_css = array('bgm-red', 'bgm-pink', 'bgm-purple', 'bgm-deeppurple', 'bgm-indigo', 'bgm-blue', 'bgm-lightblue', 'bgm-cyan', 'bgm-teal', 'bgm-green', 'bgm-lightgreen', 'bgm-amber', 'bgm-orange', 'bgm-deeporange', 'bgm-brown', 'bgm-gray', 'bgm-bluegray');
$us_states_abbrev = array(
	'AL' => 'Alabama',
	'AK' => 'Alaska',
	'AZ' => 'Arizona',
	'AR' => 'Arkansas',
	'CA' => 'California',
	'CO' => 'Colorado',
	'CT' => 'Connecticut',
	'DE' => 'Delaware',
	'DC' => 'District of Columbia',
	'FL' => 'Florida',
	'GA' => 'Georgia',
	'HI' => 'Hawaii',
	'ID' => 'Idaho',
	'IL' => 'Illinois',
	'IN' => 'Indiana',
	'IA' => 'Iowa',
	'KS' => 'Kansas',
	'KY' => 'Kentucky',
	'LA' => 'Louisiana',
	'ME' => 'Maine',
	'MD' => 'Maryland',
	'MA' => 'Massachusetts',
	'MI' => 'Michigan',
	'MN' => 'Minnesota',
	'MS' => 'Mississippi',
	'MO' => 'Missouri',
	'MT' => 'Montana',
	'NE' => 'Nebraska',
	'NV' => 'Nevada',
	'NH' => 'New Hampshire',
	'NJ' => 'New Jersey',
	'NM' => 'New Mexico',
	'NY' => 'New York',
	'NC' => 'North Carolina',
	'ND' => 'North Dakota',
	'OH' => 'Ohio',
	'OK' => 'Oklahoma',
	'OR' => 'Oregon',
	'PA' => 'Pennsylvania',
	'RI' => 'Rhode Island',
	'SC' => 'South Carolina',
	'SD' => 'South Dakota',
	'TN' => 'Tennessee',
	'TX' => 'Texas',
	'UT' => 'Utah',
	'VT' => 'Vermont',
	'VA' => 'Virginia',
	'WA' => 'Washington',
	'WV' => 'West Virginia',
	'WI' => 'Wisconsin',
	'WY' => 'Wyoming',
);
?>