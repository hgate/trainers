<?php

if($_POST['button'] == "editBasic")
{
    $STH = $asdb->prepare('UPDATE user_list 
                            SET full_name = ?, address = ?, city = ?, state = ?, zip = ?, company_name= ?
                            WHERE user_id = ?');

    $STH->execute(array($_POST['full_name'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['company_name'], $_POST['id']));
                  
                   echo "true";
}
elseif($_POST['button'] == "editContact")
{
    $STH = $asdb->prepare('UPDATE user_list
                            SET phone = ?, email = ? 
                            WHERE user_id = ?');

    $STH->execute(array($_POST['phone'], $_POST['email'], $_POST['id']));
    
    echo "true";
}

?>