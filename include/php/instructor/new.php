<?php

if($_POST['button'] == "createNew")
{
	$name=$_POST['first_name'].' '.$_POST['last_name'];
	$username=$_POST['username'];
    $password=rand();
	$email=$_POST['email'];
    $STH = $asdb->prepare('INSERT INTO trainers ( username,first_name, last_name, address, city, state, zip,  phone, email,password)
							VALUES (?,?,?,?,?,?,?,?,?,?)');
    $STH->execute(array($_POST['username'],$_POST['first_name'],$_POST['last_name'] ,$_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['phone'], $_POST['email'],$password));
	$instructor_id = $asdb->lastInsertId();
    if($_POST['image'] && $_POST['image']!='') : 
	      $file = DIR_UPLOADS."/temp/".$_POST['image'];
	      $target_dir = DIR_UPLOADS."/instructor_profile_pic/".basename("instructor-".$instructor_id.".img");
	      copy($file, $target_dir);
		  unlink($file);
	endif;   
	ob_start();
	include(DIR_PHP."/instructor/instructor_signup_mail_template.php");
	$body = ob_get_clean();
	
	require DIR_VENDORS.'/phpMailer/PHPMailerAutoload.php';
	
	$mail = new PHPMailer;
	
	$mail->setFrom('training@ablesafety.com', 'Able Safety Consulting');
	$mail->addAddress($email, $name);     // Add a recipient

    $mail->isHTML(true);   
	
	$mail->Subject = "Login Details .";
	$mail->Body    = $body;
	
	$mail->send();   
    echo "true";
}

?>