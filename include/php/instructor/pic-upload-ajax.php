<?php

$target_dir = ($_POST['target_dir'] && $_POST['target_dir']!='')?$_POST['target_dir']:DIR_UPLOADS."/temp/";


$imageFileType = pathinfo($target_dir . basename($_FILES["file-0"]["name"]),PATHINFO_EXTENSION);

$target_file = $target_dir . basename("instructor-".$_POST['code'].".img");

$uploadOk = 1;


// Check if image file is a actual image or fake image
if($_FILES) {
    $check = getimagesize($_FILES["file-0"]["tmp_name"]);

    if($check !== false) {
       // echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
       // echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    die("big");
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    die("type");
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "false";
// if everything is ok, try to upload file
} else {
    
    if (move_uploaded_file($_FILES["file-0"]["tmp_name"], $target_file)) {
        echo "true";
    } else {
        echo "false";
    }
}

?>