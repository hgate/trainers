<?php

if($_POST['button'] == "editBasic")
{
    $STH = $asdb->prepare('UPDATE trainers 
                            SET first_name = ?,last_name = ?, address = ?, city = ?, state = ?, zip = ?
                            WHERE trainer_id = ?');

    $STH->execute(array($_POST['first_name'],$_POST['last_name'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['id']));
                  
                   echo "true";
}
elseif($_POST['button'] == "editContact")
{
    $STH = $asdb->prepare('UPDATE trainers
                            SET phone = ?, email = ? 
                            WHERE trainer_id = ?');

    $STH->execute(array($_POST['phone'], $_POST['email'], $_POST['id']));
    
    echo "true";
}

?>