<?php

ob_start();
include(DIR_PHP."/courses/certificate_mail_template.php");
$body = ob_get_clean();

require DIR_VENDORS.'/phpMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$mail->setFrom('training@ablesafety.com', 'Able Safety Consulting');
$mail->addAddress($email, $name);     // Add a recipient

$mail->addAttachment(DIR_UPLOADS.'/certificates/'.$order_id.'.pdf', str_replace(' ', '', $name).'_'.str_replace(' ', '', $title).'.pdf');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = "Completed ".$title." class.";
$mail->Body    = $body;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
} 
?>