<?php
if($_POST['button'] == 'addClass'){
    $title = $_POST['title'];
    $venue_name = $_POST['venue'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $dateAndTime = serialize(explode("  ,", $_POST['dateList']));
    $cat = $_POST['cat'];
    
    $STH = $asdb->prepare('INSERT INTO private_courses ( title, venue_name, address, city, state, zip, dateAndTime, category_id)
                        VALUES (:title, :venue_name, :address, :city, :state, :zip, :dateAndTime, :cat)');
    $STH->bindParam(":title", $title, PDO::PARAM_STR);
    $STH->bindParam(":venue_name", $venue_name, PDO::PARAM_STR);
    $STH->bindParam(":address", $address, PDO::PARAM_STR);
    $STH->bindParam(":city", $city, PDO::PARAM_STR);
    $STH->bindParam(":state", $state, PDO::PARAM_STR);
    $STH->bindParam(":zip", $zip, PDO::PARAM_STR);
    $STH->bindParam(":dateAndTime", $dateAndTime, PDO::PARAM_STR);
    $STH->bindParam(":cat", $cat, PDO::PARAM_INT);
    if($STH->execute()){
        echo "true";
    } else {
        echo "false";
    }
    }
?>