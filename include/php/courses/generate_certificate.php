<?php 

$STH = $asdb->query('SELECT course_type, full_name, user_list.address, user_list.city, user_list.state, user_list.zip, first_name, last_name, user_list.email
                     FROM orders 
                     INNER JOIN user_list ON user_list.user_id = orders.student_id
                     INNER JOIN course_instructor ON orders.course_id = course_instructor.course_id AND orders.private = course_instructor.private
                     INNER JOIN trainers ON trainers.trainer_id = course_instructor.trainer_id
                     WHERE order_id="'.$order_id.'"');

$row = $STH->fetch();

$title_STH = $asdb->query('SELECT name FROM osha_terms WHERE term_id="'.$row['course_type'].'"');
$title_row = $title_STH->fetch();
$title = $title_row['name'];

$show = 0;
$law = "";
$current_date = date("m / d / Y");
if($row['course_type'] == 611 || $row['course_type'] == 602 || $row['course_type'] == 612 || $row['course_type'] == 609){
    
    $exp_date = date('m / d / Y', strtotime("+5 year"));
    
} else if($row['course_type'] == 610 || $row['course_type'] == 603 || $row['course_type'] == 624){
    
    $exp_date = date('m / d / Y', strtotime("+3 year"));
    
}
$codedate = date("y");

//epa refresher
if($row['course_type'] == 611)
{
    $TGT = $asdb->query("SELECT count FROM certificate_templates WHERE cat_id = 611 AND year=".$codedate);
    if($number = $TGT->fetch()){
        $countID = $number['count'] + 1;
        
        $UCC = $asdb->prepare('UPDATE certificate_templates SET count = :count WHERE cat_id = 611 AND year='.$codedate);
        $UCC->bindParam(":count", $countID, PDO::PARAM_INT);
        $UCC->execute();
        
    } else {
        $countID = 1;
        $UCC = $asdb->prepare("INSERT INTO certificate_templates (cat_id, count, year)
                                VALUES(611, 1, :year)");
        $UCC->bindParam(":year", intval($codedate), PDO::PARAM_INT);
        $UCC->execute();
    }
    
    $show = 1;
    
    $law = "(English) In accordance with 40 CFR Part 745";
}
//epa initial
if($row['course_type'] == 602)
{
    $TGT = $asdb->query("SELECT count FROM certificate_templates WHERE cat_id = 602 AND year=".$codedate);
    if($number = $TGT->fetch()){
        $countID = $number['count'] + 1;
        
        $UCC = $asdb->prepare('UPDATE certificate_templates SET count = :count WHERE cat_id = 602 AND year='.$codedate);
        $UCC->bindParam(":count", $countID, PDO::PARAM_INT);
        $UCC->execute();
        
    } else {
        $countID = 1;
        $UCC = $asdb->prepare("INSERT INTO certificate_templates (cat_id, count, year)
                                VALUES(602, 1, :year)");
        $UCC->bindParam(":year", intval($codedate), PDO::PARAM_INT);
        $UCC->execute();
    }
    
    $show = 1;
    
    $law = "(English) In accordance with 40 CFR Part 745.225";
}
//flagger
if($row['course_type'] == 603)
{
    $show = 0;
    
    $law = "in compliance with National Manual of Uniform Traffic Control Devices and New York State Department of Transportation";
}


//generate certificate
include(DIR_VENDORS."/mpdf/mpdf.php");

$mpdf=new mPDF('','letter-L');
$mpdf->SetImportUse(); 

$pagecount = $mpdf->SetSourceFile(DIR_PHP.'/courses/certificate_template2.pdf');

// Import the last page of the source PDF file
$tplId = $mpdf->ImportPage($pagecount);

//title border-style:solid; border-width: 2px; width:100%; 
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:25pt;'><strong>".$title."</strong></div>", 26, 99 , 227, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:16pt;'>".$law."</div>", 26, 109 , 227, 50);

//student info
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:18pt;'><strong>".strtoupper($row['full_name'])."</strong></div>", 26, $show == 1? 73:85 , 227, 50);
if($show == 1)
{
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:16pt;'>".strtoupper($row['address'])."</div>", 26, 80 , 227, 50);
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:16pt;'>".strtoupper($row['city']).", ".$row['state']." ".$row['zip']."</div>", 26, 85 , 227, 50);
}
if($row['course_type'] == 602){
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>Certificate Number: R-I-75920-".$codedate."-".str_pad($countID,5,"0",STR_PAD_LEFT)."</strong></div>", 26, 114 , 227, 50);
} else if($row['course_type'] == 611){
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>Certificate Number: R-R-75920-".$codedate."-".str_pad($countID,5,"0",STR_PAD_LEFT)."</strong></div>", 26, 114 , 227, 50);    
}

//bottom
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:brushsci; font-size:24pt;'>".$row['first_name']." ".$row['last_name']."</div>", 38, 161 , 70, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$row['first_name']." ".$row['last_name']."</div>", 38, 172 , 38, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$current_date."</div>", 130, 166 , 28, 60);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$exp_date."</div>", 223, 168 , 28, 50);

//picture
if($show == 1)
    $mpdf->Image(DIR_UPLOADS."/profile_pics/student-".$user_id.".img",189,117,50,50,'img','',true, false);

$mpdf->UseTemplate($tplId);

$mpdf->Output(DIR_UPLOADS."/certificates/".$order_id.".pdf",'F');

//mail certificate once 
$name = $row['full_name'];
$email = $row['email'];
include(DIR_PHP."/courses/mail_certificate.php");


/* original placement certificate

// Import the last page of the source PDF file
$tplId = $mpdf->ImportPage($pagecount);

//title border-style:solid; border-width: 2px; width:100%; 
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:20pt;'><strong>".$title."</strong></div>", 26, 80 , 227, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$law."</div>", 26, 87 , 227, 50);

//student info
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:16pt;'><strong>".$row['full_name']."</strong></div>", 26, 99 , 227, 50);
if($show == 1)
{
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$row['address']."</div>", 26, 105 , 227, 50);
    $mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'>".$row['city'].", ".$row['state']." ".$row['zip']."</div>", 26, 109 , 227, 50);
}
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>Order ID: ".$order_id."</strong></div>", 26, 113 , 227, 50);

//bottom
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:brushsci; font-size:24pt;'><strong>".$row['first_name']." ".$row['last_name']."</strong></div>", 38, 161 , 70, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>".$row['first_name']." ".$row['last_name']."</strong></div>", 38, 172 , 38, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>".$current_date."</strong></div>", 130, 166 , 25, 50);
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:12pt;'><strong>".$current_date."</strong></div>", 225, 167 , 28, 50);

//picture
$mpdf->WriteFixedPosHTML("<div style='width:100%; text-align:center; font-family:arial; font-size:93pt;'><strong>PIC</strong></div>", 188, 120 , 65, 50);

*/

?>

