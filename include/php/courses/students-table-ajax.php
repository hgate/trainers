<?php
$balance = $order['balance_due'];

if($_POST['payment_type'] == 'online'){
    $balance = 0;
}
elseif($_POST['payment_type'] == 'deposit'){
    $balance = ($order['balance_due'] > 0 ? ($order['balance_due'] - 50) : $order['balance_due']);
}
$STH = $asdb->prepare('UPDATE orders 
                        SET payment_type = ?, balance_due = ?
                        WHERE student_id = ? AND order_id = ? AND private =0');

$STH->execute(array($_POST['payment_type'],  $balance, $_POST['student_id'], $_POST['order_id']));

echo "true";

?>