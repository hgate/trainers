<?php
$course_id = intval($_POST['id']);

$students = $asdb->prepare('SELECT attendance, order_id, student_id, full_name, payment_type, course_type, balance_due 
                        FROM orders
                        INNER JOIN user_list ON orders.student_id = user_list.user_id
                        WHERE course_id = ?');

$students->execute(array($course_id));
$container = $students->fetchAll();

$jsonArray = array();
$jsonArray['rows'] = array();
$jsonArray['current'] = $_POST['current'];
$jsonArray['rowCount'] = $_POST['rowCount'];

$count = 0;  
foreach($container as $row) 
{
     if(true){
        $jsonArray['rows'][$count]['order_id'] = $row['order_id'];
        $jsonArray['rows'][$count]['id'] = $row['student_id'];
        $jsonArray['rows'][$count]['student'] = $row['full_name']; 


        $attendance = unserialize($row['attendance']);
        if(is_array($attendance))
        {
            foreach($attendance as $key=>$check)
            {
               $jsonArray['rows'][$count][$key] = $check;
            }
            if(!in_array(1, $attendance))
            {
               $jsonArray['rows'][$count]['commands'] = 0;
            }
            elseif(!in_array(0, $attendance))
            {
                $jsonArray['rows'][$count]['commands'] = 1;
            }
        }
        else
        {
            for($i = 0; $i < intval ($_POST['num_dates']); $i++)
            {
                $jsonArray['rows'][$count][$key] = 0;
            }
            $jsonArray['rows'][$count]['commands'] = 0;
        }

        
        $count++; 
     }
}

$jsonArray['total'] = $count;

print json_encode($jsonArray);
?>