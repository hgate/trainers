<?php

$STH = $asdb->prepare('SELECT attendance 
                        FROM orders
                        WHERE order_id = ? AND student_id = ?');

$STH->execute(array($_POST['order_id'], $_POST['student_id']));
$row = $STH->fetch();
$attendance = unserialize($row['attendance']);
$dates = unserialize($_POST['dates']);

if(!is_array( $attendance))
{

    $attendance = array_fill(0, count($dates), 0);
}

foreach($dates as $key=>$date)
{

    if($_POST['dateStr'] == substr($date, 0, 12))
        $attendance[$key] = ($attendance[$key] == 0)? 1 : 0;
        
    
}

$STH = $asdb->prepare('UPDATE orders 
                        SET attendance = ?
                        WHERE student_id = ? AND order_id = ?');

$STH->execute(array(serialize($attendance), $_POST['student_id'], $_POST['order_id']));

echo "true";

?>