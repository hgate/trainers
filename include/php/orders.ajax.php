<?php
if(isset($_GET['start']) && isset($_GET['end'])){
    $startdate = strtotime($_GET['start']);
    $enddate = strtotime($_GET['end']) + 86400;
    
    
    $STH = $asdb->query("SELECT order_date FROM orders
                        WHERE order_date BETWEEN FROM_UNIXTIME(".$startdate.") AND FROM_UNIXTIME(".$enddate.")
                        ORDER BY order_date ASC");
    $arr = array();
    while($dates = $STH->fetch(PDO::FETCH_ASSOC)){
        array_push($arr, date("Y/m/d",strtotime($dates['order_date'])));
    }
    
    $final = array();
    $test = array_count_values($arr);
    $tarr = array_unique($arr);
    foreach($tarr as $tar){

    array_push($final, array($tar, $test[$tar]));


    }

    echo json_encode($final, JSON_PRETTY_PRINT);
}
?>