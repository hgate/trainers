<?php

function creatDatabaseConnection()
{

    $dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";

    $opt = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    );

    return new PDO($dsn, DB_USER, DB_PWD, $opt);

}

function get_header()
{
    include(DIR_VIEWS."/header.php");
}

function get_footer()
{
    include(DIR_VIEWS."/footer.php");
}

function isLoggedIn()
{
    if(isset($_SESSION['user']))
        return true;
}

function isAdmin()
{
    if($_SESSION['user']['user_type'] == 0)
        return true;
}

function course_categories($col_num = 4)
{

    global $asdb, $color_css;
    
    $STH = $asdb->query('SELECT name, osha_term_taxonomy.term_taxonomy_id FROM osha_terms INNER JOIN osha_term_taxonomy ON osha_terms.term_id = osha_term_taxonomy.term_id WHERE osha_term_taxonomy.taxonomy = "tribe_events_cat" ORDER BY name ASC');
    
    $cat_div = '<div class = "row">';
    
    while($rows = $STH->fetch(PDO::FETCH_ASSOC))
    {
    
        $cat_div.='<div class=" waves-effect col-sm-'.$col_num.'">
            <a href = "'.SITE_URL."/courses/category?id=".$rows['term_taxonomy_id'].'">
            <div class="card">
                <div class="card-header '.$color_css[array_rand($color_css, 1)].'">
                    <h2>'.$rows['name'].'</h2>
                </div> 
            </div></a>
        </div>';
        
    }
    
    $cat_div.='</div>';
    
    return $cat_div;

}
function student_upcoming_courses($id)
{

    global $asdb, $color_css;
    
    $STH = $asdb->query("SELECT osha_postmeta.meta_value, osha_posts.ID, osha_posts.post_title
                            FROM osha_postmeta
                            INNER JOIN osha_posts ON osha_posts.ID = osha_postmeta.post_id
                            INNER JOIN orders ON orders.course_id = osha_posts.ID
                            WHERE orders.student_id ='".$id."'
                            AND osha_postmeta.meta_key = 'dateAndTime'
                            ORDER BY osha_posts.ID DESC");
    
    $cat_div = '<div class = "row">';
    
    while($rows = $STH->fetch(PDO::FETCH_ASSOC))
    {
        $temp = unserialize($rows['meta_value']);
        $cat_div.='<div class=" waves-effect col-sm-4">
            <a href = "'.SITE_URL."/courses/?id=".$rows['ID'].'">
            <div class="card">
                <div class="card-header">
                    <h2>'.$rows['post_title'].'</h2>
                </div>
                <div class="card-body card-padding">
                    <small>Start Time:</small>
                    <small>'.$temp[0].'</small></br>
                    <small>Event ID: '.$rows['ID'].'</small>
                </div>
            </div></a>
        </div>';
        
    }
    
    $cat_div.='</div>';
    
    return $cat_div;

}
function category_courses($id, $private = false)
{
  
    global $asdb;
    
    if(!$private)
    {
        $STH = $asdb->query("SELECT osha_postmeta.meta_value, osha_posts.ID, osha_posts.post_title
                                FROM osha_postmeta
                                INNER JOIN osha_posts ON osha_posts.ID = osha_postmeta.post_id
                                INNER JOIN osha_term_relationships ON osha_term_relationships.object_id = osha_posts.ID
                                WHERE osha_term_relationships.term_taxonomy_id ='".$id."'
                                AND osha_postmeta.meta_key = 'dateAndTime'
                                ORDER BY osha_posts.ID DESC");
    
        while($rows = $STH->fetch(PDO::FETCH_ASSOC))
        {
            $cat_div .= '<tr>';
            $temp = unserialize($rows['meta_value']);
            $cat_div.='<td>'.$rows['ID'].'</td>
                    <td>'.$rows['post_title'].'</td>
                    <td>'.$temp[0].'</td>
                    ';
            $cat_div.='</div>';

        }
    }
    else
    {
        $STH = $asdb->query("SELECT dateAndTime, id, title
                                FROM private_courses
                                WHERE category_id ='".$id."'
                                ORDER BY id DESC");
        while($rows = $STH->fetch(PDO::FETCH_ASSOC))
        {
            $cat_div .= '<tr>';
            $temp = unserialize($rows['dateAndTime']);
            $cat_div.='<td>'.$rows['id'].'</td>
                    <td>'.$rows['title'].'</td>
                    <td>'.$temp[0].'</td>
                    ';
            $cat_div.='</div>';

        }
    }
    


    
    return $cat_div;

}



?>